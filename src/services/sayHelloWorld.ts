export const sayHello = (name='') => {
    return `Hello ${name ? name : 'World'}!`
}