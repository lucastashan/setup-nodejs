import { sayHello } from './sayHelloWorld';

describe('Say Hello tests', () => {
    it('Should say Hello World', () => {
        const response = sayHello();

        expect(response).toBe('Hello World!');
    })
    it('Should say Hello Tashan', () => {
        const response = sayHello('Tashan');

        expect(response).toBe('Hello Tashan!');
    })
})